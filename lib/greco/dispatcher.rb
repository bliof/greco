games ||= []

get '/' do
  games << Greco::Chess::Game.new

  redirect "/game/#{games.size - 1}/"
end

get '/game/:id/' do
  game = games[params[:id].to_i]

  return redirect "/" unless game

  board = game.board.map do |field|
    unless field.empty?
      piece = field.piece
      symbol = case piece
        when Greco::Chess::Piece::King
          piece.white? ? '&#9812;' : '&#9818;'
        when Greco::Chess::Piece::Queen
          piece.white? ? '&#9813;' : '&#9819;'
        when Greco::Chess::Piece::Rook
          piece.white? ? '&#9814;' : '&#9820;'
        when Greco::Chess::Piece::Pawn
          piece.white? ? '&#9817;' : '&#9823;'
        when Greco::Chess::Piece::Knight
          piece.white? ? '&#9816;' : '&#9822;'
        when Greco::Chess::Piece::Bishop
          piece.white? ? '&#9815;' : '&#9821;'
      end

      "<a href='#'>#{symbol}</a>"
    end
  end

  erb :chessboard, locals: { board: board, result: game.result, player: game.scoresheet.player_on_move }
end

get '/game/:id/move' do
  game = games[params[:id].to_i]

  game.make_move Greco::Chess::Move.new(params[:from].downcase.to_sym, params[:to].downcase.to_sym)

  redirect "/game/#{params[:id]}/"
end
