module Greco
  module Chess
    class Validator
      attr_reader :board, :move

      def initialize(board, move)
        @board = board
        @scoresheet = board.scoresheet
        @move = move
        @from = @board[@move.from]
        @to = @board[@move.to]
        @piece = @from.piece

        @enemy = oposite_color(@from.piece.color) if @from.piece
      end

      def check?
        @error = nil

        return false unless @from and @to

        !!(
          moves_a_piece? and
          right_color_to_play_with? and
          valid_piece_move? and
          valid_promotion? and
          !is_the_king_left_under_attack?
        )
      end

      private

      def is_the_king_left_under_attack?
        @board.apply_move move

        king = @board.get_king_of @piece.color

        result = king.field.attacked_by? @enemy

        @board.undo_last_move

        result
      end

      def oposite_color(color)
        return unless color

        if color == :white
          :black
        else
          :white
        end
      end

      def moves_a_piece?
        !@from.empty?
      end

      def right_color_to_play_with?
        @scoresheet.player_on_move == @piece.color
      end

      def valid_piece_move?
        case @piece
          when Piece::Pawn
            valid_pawn_move?
          when Piece::King
            valid_king_move?
          else
            valid_basic_piece_move?
        end
      end

      def valid_basic_piece_move?
        @piece.attacks? @to and (@to.empty? or @to.piece.color != @piece.color)
      end

      def valid_king_move?
        if valid_basic_piece_move?
          true
        else
          valid_castle?
        end
      end

      def valid_castle?
        return false if @piece.moved? or @from.attacked_by? @enemy

        valid_short_castle? || valid_long_castle?
      end

      def valid_short_castle?
        if @piece.white?
          @from == :e1 and @to == :g1 and
            @board[:f1].empty? and @board[:g1].empty? and
            @board[:h1].piece.is_a?(Piece::Rook) and
            not @board[:h1].piece.moved? and
            not @board[:f1].attacked_by? @enemy
        else
          @from == :e8 and @to == :g8 and
            @board[:f8].empty? and @board[:g8].empty? and
            @board[:h8].piece.is_a?(Piece::Rook) and
            not @board[:h8].piece.moved? and
            not @board[:f8].attacked_by? @enemy
        end
      end

      def valid_long_castle?
        if @piece.white?
          @from == :e1 and @to == :c1 and
          @board[:d1].empty? and @board[:c1].empty? and @board[:b1].empty? and
          @board[:a1].piece.is_a?(Piece::Rook) and
          not @board[:a1].piece.moved? and
          not @board[:d1].attacked_by? @enemy
        else
          @from == :e8 and @to == :c8 and
          @board[:d8].empty? and @board[:c8].empty? and @board[:b8].empty? and
          @board[:a8].piece.is_a?(Piece::Rook) and
          not @board[:a8].piece.moved? and
          not @board[:d8].attacked_by? @enemy
        end
      end

      def valid_pawn_move?
        move_from = @from.coordinates.to_120
        move_to = @to.coordinates.to_120
        step = move_to - move_from

        if @piece.white?
          return false unless step < 0
        else
          return false unless step > 0
        end

        if step.abs == 9 or step.abs == 11
          if @to.piece
            @to.piece.color != @piece.color
          else
            valid_en_passant?
          end
        elsif step.abs == 10
          @to.empty?
        elsif step.abs == 20
          return false unless (@piece.white? and move_from.between? 81, 88) or
                              (@piece.black? and move_from.between? 31, 38)
          upper_field = board[move_from + step/2]
          @to.empty? and upper_field.empty?
        else
          true
        end
      end

      def valid_en_passant?
        move_to = @to.coordinates.to_120

        meta_move = MetaMove.new @move, @board.scoresheet.last_move
        meta_move.piece = @piece

        if @piece.white?
          meta_move.captured = @board[move_to + 10].piece
        else
          meta_move.captured = @board[move_to - 10].piece
        end

        meta_move.is_en_passant?
      end

      def valid_promotion?
        return true unless @move.promote
        move_to = @to.coordinates.to_120

        return false if @piece.color != @move.promote.color or !@piece.is_a?(Piece::Pawn)

        if @piece.white?
          move_to.between? 21, 28
        else
          move_to.between? 91, 98
        end
      end
    end
  end
end
