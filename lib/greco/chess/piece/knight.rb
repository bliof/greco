module Greco
  module Chess
    module Piece
      class Knight < BasePiece
        include BasicMovement

        def attacks
          board = @board.to_120
          pos = @field.coordinates.to_120

          board.values_at(
            pos - 12, pos - 21, pos - 19, pos - 8,
            pos + 12, pos + 21, pos + 19, pos + 8,
          ).select { |current| current }
        end

        alias :moves :basic_moves
      end
    end
  end
end
