module Greco
  module Chess
    module Piece
      class Pawn < BasePiece
        include BasicMovement

        def attacks
          pos = @field.coordinates.to_120
          board = @board.to_120

          result = []

          upper_left = pos + (white? ? -11 : 9)
          upper_right = pos + (white? ? -9 : 11)

          result << board[upper_left] unless board[upper_left].nil?
          result << board[upper_right] unless board[upper_right].nil?

          result
        end

        def moves
          result = basic_moves

          pos = @field.coordinates.to_120

          other_posible_moves = []

          if white?
            other_posible_moves << Move.new(pos, pos - 10) << Move.new(pos, pos - 20)
          else
            other_posible_moves << Move.new(pos, pos + 10) << Move.new(pos, pos + 20)
          end

          other_posible_moves.each do |move|
            validator = Validator.new @board, move
            result << @board[move.to] if validator.check?
          end

          result
        end
      end
    end
  end
end
