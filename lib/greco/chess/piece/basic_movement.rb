module Greco
  module Chess
    module BasicMovement
      def basic_moves
        attacks.select do |current|
          validator = Validator.new @board, Move.new(@field.coordinates, current.coordinates)
          validator.check?
        end
      end
    end
  end
end
