module Greco
  module Chess
    module Piece
      module SlidingAttack
        def attacks_when_sliding
          pos = @field.coordinates

          attacked_fields = []

          @sliding_steps.each do |step|
            fields = @board.fields_from pos, step

            nonblocked_fields = fields.take_while &:empty?

            attacked_fields += nonblocked_fields

            if fields[nonblocked_fields.size]
              attacked_fields << fields[nonblocked_fields.size]
            end
          end

          attacked_fields
        end
      end
    end
  end
end
