module Greco
  module Chess
    module Piece
      class King < BasePiece
        include BasicMovement

        def attacks
          board = @board.to_120
          pos = @field.coordinates.to_120

          board.values_at(
            pos - 11, pos - 10, pos - 9,
            pos - 1,            pos + 1,
            pos + 9,  pos + 10, pos + 11
          ).select { |current| current }
        end

        def moves
          result = basic_moves

          pos = @field.coordinates

          other_posible_moves = []

          if white?
            return result unless pos == :e1
            other_posible_moves << Move.new(:e1, :c1) << Move.new(:e1, :g1)
          else
            return result unless pos == :e8
            other_posible_moves << Move.new(:e8, :c8) << Move.new(:e8, :g8)
          end

          other_posible_moves.each do |move|
            validator = Validator.new @board, move
            result << @board[move.to] if validator.check?
          end

          result
        end
      end
    end
  end
end
