module Greco
  module Chess
    module Piece
      class Rook < BasePiece
        include SlidingAttack
        include BasicMovement

        def initialize(*params)
          super *params

          @sliding_steps = [10, 1, -10, -1]
        end

        alias :attacks :attacks_when_sliding
        alias :moves :basic_moves
      end
    end
  end
end
