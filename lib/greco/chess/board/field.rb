module Greco
  module Chess
    class Board
      class Field
        attr_reader :board, :coordinates, :piece

        def initialize(board, coordinates, piece = nil)
          @board = board
          @coordinates = Coordinates.new coordinates
          @piece = piece
        end

        def empty?
          @piece.nil?
        end

        def set_piece(piece)
          @piece = piece
        end

        def piece=(piece)
          if @piece
            @piece.set_field nil
          end

          if piece.nil?
            @piece = nil
          else
            piece.field = self
          end
        end

        def ==(other)
          if other.is_a? Field
            self.coordinates == other.coordinates and
            self.piece.class == other.piece.class
          else
            self.coordinates == other
          end
        end

        def attacked_by?(color)
          if color == :white
            @board.white_pieces.any? { |piece| piece.attacks? @coordinates }
          else
            @board.black_pieces.any? { |piece| piece.attacks? @coordinates }
          end
        end

        def to_s
          "Field on #{coordinates.to_an} " + (empty? ? 'without piece' : "with #{@piece}")
        end
      end
    end
  end
end
