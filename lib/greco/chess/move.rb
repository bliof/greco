module Greco
  module Chess
    class Move
      attr_reader :from, :to, :promote

      def initialize(from, to, promote = nil)
        @from = Coordinates.new from
        @to = Coordinates.new to
        @promote = promote
      end
    end
  end
end
