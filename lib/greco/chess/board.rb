#XXX This is a magical class!!!
module Greco
  module Chess
# Board:
# ====================================== ======================================
# ||==================================|| ||==================================||
# ||                                  || ||                                  ||
# ||  21  22  23  24  25  26  27  28  || ||  a8  b8  c8  d8  e8  f8  g8  h8  ||
# ||                                  || ||                                  ||
# ||  31  32  33  34  35  36  37  38  || ||  a7  b7  c7  d7  e7  f7  g7  h7  ||
# ||                                  || ||                                  ||
# ||  41  42  43  44  45  46  47  48  || ||  a6  b6  c6  d6  e6  f6  g6  h6  ||
# ||                                  || ||                                  ||
# ||  51  52  53  54  55  56  57  58  || ||  a5  b5  c5  d5  e5  f5  g5  h5  ||
# ||                                  || ||                                  ||
# ||  61  62  63  64  65  66  67  68  || ||  a4  b4  c4  d4  e4  f4  g4  h4  ||
# ||                                  || ||                                  ||
# ||  71  72  73  74  75  76  77  78  || ||  a3  b3  c3  d3  e3  f3  g3  h3  ||
# ||                                  || ||                                  ||
# ||  81  82  83  84  85  86  87  88  || ||  a2  b2  c2  d2  e2  f2  g2  h2  ||
# ||                                  || ||                                  ||
# ||  91  92  93  94  95  96  97  98  || ||  a1  b1  c1  d1  e1  f1  g1  h1  ||
# ||                                  || ||                                  ||
# ||==================================|| ||==================================||
# ====================================== ======================================
#
    class Board
      class << self
        def create_default_board
          Board.new a8: Piece::Rook.new(:black),
                    b8: Piece::Knight.new(:black),
                    c8: Piece::Bishop.new(:black),
                    d8: Piece::Queen.new(:black),
                    e8: Piece::King.new(:black),
                    f8: Piece::Bishop.new(:black),
                    g8: Piece::Knight.new(:black),
                    h8: Piece::Rook.new(:black),
                    a7: Piece::Pawn.new(:black),
                    b7: Piece::Pawn.new(:black),
                    c7: Piece::Pawn.new(:black),
                    d7: Piece::Pawn.new(:black),
                    e7: Piece::Pawn.new(:black),
                    f7: Piece::Pawn.new(:black),
                    g7: Piece::Pawn.new(:black),
                    h7: Piece::Pawn.new(:black),
                    a1: Piece::Rook.new(:white),
                    b1: Piece::Knight.new(:white),
                    c1: Piece::Bishop.new(:white),
                    d1: Piece::Queen.new(:white),
                    e1: Piece::King.new(:white),
                    f1: Piece::Bishop.new(:white),
                    g1: Piece::Knight.new(:white),
                    h1: Piece::Rook.new(:white),
                    a2: Piece::Pawn.new(:white),
                    b2: Piece::Pawn.new(:white),
                    c2: Piece::Pawn.new(:white),
                    d2: Piece::Pawn.new(:white),
                    e2: Piece::Pawn.new(:white),
                    f2: Piece::Pawn.new(:white),
                    g2: Piece::Pawn.new(:white),
                    h2: Piece::Pawn.new(:white)
        end
      end

      include Enumerable

      attr_accessor :scoresheet

      def initialize(fields = {}, scoresheet = nil)
        @board = Array.new(120)
        2.upto(9) do |n|
          a_column = n * 10 + 1
          h_column = a_column + 7

          a_column.upto(h_column) do |current|
            @board[current] = Field.new self, current
          end
        end

        set fields

        @scoresheet = scoresheet || Scoresheet.new
      end

      def each(&block)
        to_64.each(&block)
      end

      def pieces
        map(&:piece).select { |piece| piece }
      end

      def black_pieces
        pieces.select { |piece| piece.color == :black }
      end

      def white_pieces
        pieces.select { |piece| piece.color == :white }
      end

      def [](coordinates)
        pos = Coordinates.new(coordinates).to_120
        @board[pos]
      end

      def get(*wanted)
        positions = wanted.map { |current| Coordinates.new(current).to_120 }

        @board.values_at *positions
      end

      def set(fields = {})
        fields.each do |coordinates, piece|
          pos = Coordinates.new(coordinates).to_120

          @board[pos].piece = piece
        end
      end

      def upper_left_diagonal_from(field)
        fields_from field, -9
      end

      def lower_left_diagonal_from(field)
        fields_from field, 11
      end

      def upper_right_diagonal_from(field)
        fields_from field, -11
      end

      def lower_right_diagonal_from(field)
        fields_from field, 9
      end

      def up_from(field)
        fields_from field, -10
      end

      def right_from(field)
        fields_from field, 1
      end

      def down_from(field)
        fields_from field, 10
      end

      def left_from(field)
        fields_from field, -1
      end

      def to_120
        @board
      end

      def to_64
        result = []
        2.upto(9) do |n|
          a_column = n * 10 + 1
          h_column = a_column + 7
          result += @board.slice(a_column .. h_column)
        end

        result
      end

      def fields_from(field, step)
        current = Coordinates.new(field).to_120 + step
        result = []

        while @board[current]
          result << @board[current]
          current += step
        end

        result
      end

      def get_king_of(color)
        if color == :white
          white_pieces.find { |piece| piece.is_a? Piece::King }
        else
          black_pieces.find { |piece| piece.is_a? Piece::King }
        end
      end

      def apply_move(move)
        move_from = move.from.to_120
        move_to = move.to.to_120

        meta_move = MetaMove.new move

        piece = @board[move_from].piece

        meta_move.piece = piece
        meta_move.captured = @board[move_to].piece
        @board[move_to].piece = piece

        piece.times_moved += 1

        @scoresheet.add_move meta_move

        if meta_move.is_castling?
          apply_castling meta_move
        elsif meta_move.looks_like_en_passant?
          apply_en_passant meta_move
        elsif move.promote
          @board[move_to].piece = move.promote
        end
      end

      def undo_last_move
        move = @scoresheet.pop_move or return

        old_from = move.from.to_120
        old_to = move.to.to_120

        piece = move.piece
        piece.times_moved -= 1

        @board[old_from].piece = piece

        if move.is_en_passant?
          undo_en_passant move
        elsif move.captured
          @board[old_to].piece = move.captured
        elsif move.promote
          @board[old_to].piece = nil
        elsif move.is_castling?
          undo_castling move
        end

        move
      end

      private

      def apply_castling(move)
        if move.to == :g1
          @board[96].piece = @board[98].piece
        elsif move.to == :c1
          @board[94].piece = @board[91].piece
        elsif move.to == :g8
          @board[26].piece = @board[28].piece
        elsif move.to == :c8
          @board[24].piece = @board[21].piece
        end
      end

      def undo_castling(move)
        if move.to == :g1
          @board[98].piece = @board[96].piece
        elsif move.to == :c1
          @board[91].piece = @board[94].piece
        elsif move.to == :g8
          @board[28].piece = @board[26].piece
        elsif move.to == :c8
          @board[21].piece = @board[24].piece
        end
      end

      def apply_en_passant(move)
        previous_move_to = move.previous_move.to.to_120
        move.captured = @board[previous_move_to].piece
        @board[previous_move_to].piece = nil
      end

      def undo_en_passant(move)
        previous_move_to = move.previous_move.to.to_120
        @board[previous_move_to].piece = move.captured
      end
    end
  end
end
