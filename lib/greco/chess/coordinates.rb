module Greco
  module Chess
    class Coordinates
      def initialize(value)
        case value
          when Numeric
            @value_120 = value
            @value_an = Notation::Algebraic::from_internal_coordinates value
          when Symbol
            @value_120 = Notation::Algebraic::to_internal_coordinates value
            @value_an = value
          when Coordinates
            @value_120 = value.to_120
            @value_an = value.to_an
          else
            raise ArgumentError "bad value for coordinates - #{value.class}"
        end
      end

      def to_an
        @value_an
      end

      def to_120
        @value_120
      end

      def ==(other)
        case other
          when Numeric
            self.to_120 == other
          when Symbol
            self.to_an == other
          when Coordinates
            self.to_120 == other.to_120
          else
            false
        end
      end
    end
  end
end
