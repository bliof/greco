module Greco
  module Chess
    module Notation
      class Algebraic
        class << self
          COLUMNS_AN  = 'abcdefgh'
          COLUMNS_120 = '12345678'
          ROWS_AN     = '12345678'
          ROWS_120    = '98765432'

          def to_internal_coordinates(symbol)
            symbol.to_s.reverse.tr(
              COLUMNS_AN + ROWS_AN, COLUMNS_120 + ROWS_120
            ).to_i
          end

          def from_internal_coordinates(number)
            column = (number % 10).to_s
            row = (number / 10).to_s

            result = column.tr COLUMNS_120, COLUMNS_AN
            result += row.tr ROWS_120, ROWS_AN

            result.to_sym
          end
        end
      end
    end
  end
end
