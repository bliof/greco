watch(%r{(lib|spec)/*}) do |m|
  system "clear"
  puts '=' * 80
  puts Time.now
  puts '=' * 80
  success = system "rake test"

  if (success)
    `tmux set-option -g status-bg colour22`
  else
    `tmux set-option -g status-bg colour52`
  end
end

Signal.trap('INT') do
  `tmux set-option -g status-bg colour234`
  puts "\n"
  exit
end
