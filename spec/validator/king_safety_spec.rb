require 'spec_helper'

module Greco
  module Chess
    describe Validator do
      include TestUtils

      describe 'The king should not be under attack on the next move' do
        it do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- bB -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          is_not_ok_to_move? board, Move.new(:e1, :e2)
        end

        it do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- bR -- wR wK -- -- --
          )

          is_not_ok_to_move? board, Move.new(:d1, :d2)
        end

        it do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- bP -- -- -- -- --
            -- -- -- bN -- -- -- --
            -- -- -- -- wK -- -- --
          )

          is_not_ok_to_move? board, Move.new(:e1, :d2)
        end

        it do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- bQ wK -- -- --
          )

          is_not_ok_to_move? board, Move.new(:e1, :d2)
          is_not_ok_to_move? board, Move.new(:e1, :e2)
          is_not_ok_to_move? board, Move.new(:e1, :f1)
          is_ok_to_move? board, Move.new(:e1, :f2)
          is_ok_to_move? board, Move.new(:e1, :d1)
        end
      end
    end
  end
end
