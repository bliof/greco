require 'spec_helper'

module Greco
  module Chess
    module Notation
      describe Algebraic do
        it '.to_internal_coordinates' do
          Algebraic::to_internal_coordinates(:b7).should eq 32
          Algebraic::to_internal_coordinates(:h2).should eq 88
          Algebraic::to_internal_coordinates(:g3).should eq 77
          Algebraic::to_internal_coordinates(:c3).should eq 73
        end

        it '.from_internal_coordinates' do
          Algebraic::from_internal_coordinates(32).should eq :b7
          Algebraic::from_internal_coordinates(44).should eq :d6
        end
      end
    end
  end
end
