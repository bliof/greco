require 'spec_helper'

module Greco
  module Chess
    module Piece
      describe King do
        include TestUtils

        describe '#attacks' do
          let(:board) { Board.new }

          it 'around itself' do
            king = King.new :white
            board.set e2: king

            king.attacks.should include :d3, :e3, :f3,
                                        :d2,      :f2,
                                        :d1, :e1, :f1
            king.attacks.size.should be 8
          end

          it 'but not outside the board' do
            king = King.new :black
            board.set e1: king

            king.attacks.should include :d1, :d2, :e2, :f2, :f1
            king.attacks.size.should be 5
          end
        end

        describe '#moves' do
          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- bP -- -- --
              -- -- -- bN bR -- -- --
              -- -- -- -- wK -- -- --
            )

            possible_moves = board[:e1].piece.moves

            possible_moves.should include :e2, :d1
            possible_moves.size.should be 2
          end

          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- bP -- -- --
              -- -- -- bN -- -- -- --
              wR -- -- -- wK -- -- wR
            )

            possible_moves = board[:e1].piece.moves

            possible_moves.should include :e2, :d1, :c1
            possible_moves.size.should be 3
          end
        end
      end
    end
  end
end
