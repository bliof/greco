require 'spec_helper'

module Greco
  module Chess
    module Piece
      describe Rook do
        include TestUtils

        describe '#attacks' do
          let (:board) { Board.new }

          it '14 fields when not blocked' do
            rook = Rook.new :black
            board.set c4: rook

            rook.attacks.should include :a4, :b4, :d4, :e4, :f4, :g4, :h4,
                                        :c8, :c7, :c6, :c5, :c3, :c2, :c1

            rook.attacks.size.should be 14
          end

          it 'till another figure is hit' do
            rook = Rook.new :white
            board.set e3: rook, e4: Pawn.new(:white), d3: Pawn.new(:black)

            rook.attacks.should include :d3, :e4, :f3, :g3, :h3, :e2, :e1
            rook.attacks.size.should be 7
          end
        end

        describe '#moves' do
          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              bR -- wR -- wK -- -- --
            )

            possible_moves = board[:c1].piece.moves

            possible_moves.should include :a1, :b1, :d1
            possible_moves.size.should be 3
          end
        end
      end
    end
  end
end
